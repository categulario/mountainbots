const path = require('path');
const fs = require('fs/promises');

const puppeteer = require('puppeteer');
const { program } = require('commander');

program
  .option('--container')
  .requiredOption('-c --cache-path <path>', 'Path to the cache folder')
  .requiredOption('-m --mountain <code>', 'Mountain code')
  .requiredOption('-a --altitude <msnm>', 'Altitude of the forecast');

program.parse();

const opts = program.opts();

(async () => {
  let args = [];

  if (opts.container) {
    args = [
      '--disable-gpu',
      '--disable-setuid-sandbox',
      '--no-sandbox',
    ];
  }

  const browser = await puppeteer.launch({
    headless: true,
    defaultViewport: {
      width: 1910,
      height: 1080,
    },
    args: args,
  });
  const page = await browser.newPage();

  // Set timeout to 60 seconds
  page.setDefaultTimeout(60 * 1000);

  // set metric system for temperature
  await page.setCookie({
    name: '_fcunits',
    value: 'temp.met_len.met_spd.met',
    domain: 'www.mountain-forecast.com',
  });

  await page.goto(`https://www.mountain-forecast.com/peaks/${opts.mountain}/forecasts/${opts.altitude}`);

  const box = await page.evaluate(() => {
    const rect = document.getElementById('forecast-table').getBoundingClientRect();

    return {
      x: rect.x,
      y: rect.y,
      width: rect.width,
      height: rect.height,
    };
  });
  const updateTime = await page.evaluate(() => {
    const hours = parseInt(document.querySelector('.issued__updates .hours').textContent);
    const minutes = parseInt(document.querySelector('.issued__updates .minutes').textContent);
    const seconds = parseInt(document.querySelector('.issued__updates .seconds').textContent);

    return hours * 60 * 60 + minutes * 60 + seconds;
  });

  const parentDir = path.join(opts.cachePath, opts.mountain);

  await fs.mkdir(parentDir, { recursive: true });

  await page.screenshot({
    clip: box,
    path: path.join(parentDir, `${opts.altitude}.png`),
  });

  console.log(JSON.stringify({
    update_time: updateTime,
  }));

  await browser.close();
})();
