use std::fmt;
use std::cmp::{PartialOrd, Ord, Ordering};
use std::collections::HashMap;

use serde::{Serialize, Deserialize};
use sha2::{Sha256, Digest};

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct MountainId(String);

impl MountainId {
    pub fn from_code(code: &str) -> MountainId {
        let mut hasher = Sha256::new();

        hasher.update(code.as_bytes());
        let hash = format!("{:x}", hasher.finalize());

        MountainId(hash[..10].to_string())
    }
}

impl From<&str> for MountainId {
    fn from(s: &str) -> MountainId {
        MountainId(s.to_string())
    }
}

impl fmt::Display for MountainId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&self.0)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Mountain {
    pub id: MountainId,
    pub name: String,
    pub normalized_name: String,
    pub code: String,
    pub altitudes: Vec<u16>,
}

impl PartialOrd for Mountain {
    fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
        self.name.partial_cmp(&rhs.name)
    }
}

impl Ord for Mountain {
    fn cmp(&self, rhs: &Self) -> Ordering {
        self.name.cmp(&rhs.name)
    }
}

impl fmt::Display for Mountain {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&self.name)
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct MountainFile {
    pub mountains: HashMap<MountainId, Mountain>,
}

impl MountainFile {
    pub fn new() -> Self {
        Self {
            mountains: HashMap::new(),
        }
    }

    pub fn from_mountains(mountains: impl IntoIterator<Item=Mountain>) -> MountainFile {
        MountainFile {
            mountains: mountains.into_iter().map(|m| (m.id.clone(), m)).collect(),
        }
    }

    pub fn by_id(&self, id: &MountainId) -> Option<&Mountain> {
        self.mountains.get(id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn some_mountian_ids() {
        assert_eq!(MountainId::from_code("mountain-code"), MountainId(String::from("9b46ef1cd1")));
    }
}
