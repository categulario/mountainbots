use std::env;
use std::fs::read_to_string;
use std::collections::HashMap;

use serde::Deserialize;

use mountains::models::MountainId;

#[derive(Debug, Clone, PartialEq, Deserialize)]
struct OldMountain {
    pub name: String,
    pub normalized_name: String,
    pub code: String,
    pub altitudes: Vec<u16>,
}

fn main() {
    let mut args = env::args();
    args.next().unwrap();

    let filename = args.next().expect("need an argument");
    let contents = read_to_string(filename).unwrap();
    let mountains: Vec<OldMountain> = serde_json::from_str(&contents).unwrap();
    let mut ids = HashMap::new();

    for mountain in mountains {
        let id = MountainId::from_code(&mountain.code);

        if let Some(m) = ids.insert(id.clone(), mountain.clone()) {
            println!("Duplicated id for mountains {} and {}: {id}", m.name, mountain.name);
        }
    }
}
