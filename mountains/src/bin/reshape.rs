use std::env;
use std::fs::read_to_string;

use serde::Deserialize;

use mountains::models::{Mountain, MountainId, MountainFile};

#[derive(Debug, Clone, PartialEq, Deserialize)]
struct OldMountain {
    pub name: String,
    pub normalized_name: String,
    pub code: String,
    pub altitudes: Vec<u16>,
}

fn main() {
    let mut args = env::args();
    args.next().unwrap();

    let filename = args.next().expect("need an argument");
    let contents = read_to_string(filename).unwrap();
    let mountains: Vec<OldMountain> = serde_json::from_str(&contents).unwrap();

    let new_mountains = MountainFile::from_mountains(mountains.into_iter().map(|m| Mountain {
        id: MountainId::from_code(&m.code),
        name: m.name,
        normalized_name: m.normalized_name,
        code: m.code,
        altitudes: m.altitudes,
    }));

    println!("{}", serde_json::to_string(&new_mountains).unwrap());
}
