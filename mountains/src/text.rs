use unidecode::unidecode;

use crate::models::{Mountain, MountainFile};

/// return a normalized version of s for the purposes of matching.
///
/// This processes are considered:
/// - lowercase
/// - remove accents or turn them into ascii chars if possible
/// - remove symbols
pub fn normalize(s: &str) -> String {
    unidecode(s).to_lowercase().replace(|c: char| {
        !c.is_alphanumeric()
    }, "")
}

pub enum Match<'a> {
    None,
    Single(&'a Mountain),
    Multiple(Vec<&'a Mountain>),
}

pub fn r#match<'a>(query: &str, mountains: &'a MountainFile) -> Match<'a> {
    let matches: Vec<_> = mountains.mountains.values()
        .filter(|m| m.normalized_name.contains(&normalize(query)))
        .collect();

    match &matches[..] {
        [] => Match::None,
        [mountain] => Match::Single(mountain.to_owned()),
        multiple => Match::Multiple(multiple.to_vec()),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn normalization() {
        assert_eq!(normalize("ándalE tu"), "andaletu");
    }
}
