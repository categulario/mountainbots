use std::path::PathBuf;

use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct Settings {
    pub node_path: PathBuf,
    pub capture_cache_dir: PathBuf,
    pub capture_script_path: PathBuf,
}

impl Default for Settings {
    fn default() -> Settings {
        Settings {
            node_path: PathBuf::from("/usr/bin/node"),
            capture_cache_dir: PathBuf::from("/tmp"),
            capture_script_path: PathBuf::from("capture/main.js"),
        }
    }
}
