use std::path::PathBuf;
use std::time::Duration;

use serde::Deserialize;
use tokio::process::Command;

pub mod models;
pub mod settings;
pub mod error;
pub mod text;

use models::Mountain;
use settings::Settings;
use error::{Error, Result};

#[derive(Debug, Deserialize)]
struct CaptureResponse {
    update_time: u64,
}

pub async fn get_capture(mountain: &Mountain, altitude: u16, settings: &Settings) -> Result<PathBuf> {
    let capture_path = {
        let mut p = settings.capture_cache_dir
            .join(&mountain.code).join(altitude.to_string());
        p.set_extension("png");
        p
    };

    if !capture_path.exists() {
        log::debug!("Calling capture module to get capture of: {capture_path:?}");

        let cmd = Command::new(&settings.node_path)
            .arg(&settings.capture_script_path)
            .arg("-c").arg(&settings.capture_cache_dir)
            .arg("-m").arg(&mountain.code)
            .arg("-a").arg(altitude.to_string())
            .arg("--container")
            .output().await.unwrap();

        let stdout = String::from_utf8_lossy(&cmd.stdout).into_owned();
        let stderr = String::from_utf8_lossy(&cmd.stderr).into_owned();

        if cmd.status.success() {
            let res: CaptureResponse = serde_json::from_str(&stdout)
                .map_err(|error| Error::DeserializingCapModOutput {
                    error,
                    output: stdout,
                })?;
            let cache_path = capture_path.clone();

            tokio::spawn(async move {
                let update_time = res.update_time;
                log::debug!("Timer set to clear cache for {cache_path:?} in {update_time} secs");
                tokio::time::sleep(Duration::from_secs(update_time)).await;

                if let Err(e) = tokio::fs::remove_file(&cache_path).await {
                    log::error!("Error removing cache at {cache_path:?}: {e}");
                } else {
                    log::debug!("Cleared cache for {cache_path:?}");
                }
            });
        } else {
            return Err(Error::CapModFailed {
                code: cmd.status.code().unwrap(),
                stdout, stderr,
            });
        }
    } else {
        log::debug!("Using cached capture: {capture_path:?}");
    }

    Ok(capture_path)
}
