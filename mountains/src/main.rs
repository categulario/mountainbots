use std::path::PathBuf;
use std::sync::Arc;

use scraper::{Html, Selector};
use reqwest::Client;
use tokio::fs;
use tokio::sync::Semaphore;
use futures::future::try_join_all;
use clap::Parser;
use thiserror::Error;

use mountains::models::{Mountain, MountainId, MountainFile};
use mountains::text::normalize;

const SITE_URL: &str = "https://www.mountain-forecast.com";
const MAX_CONCURRENT_REQUESTS: usize = 20;

#[derive(Debug, Error)]
enum ScrapError {
    #[error("Page at url {0} didn't respond to the name selector")]
    NameSelectorFailed(String),

    #[error("Request failed trying to get page for mountain: {0}")]
    MountainRequest(#[from] reqwest::Error),

    #[error("IO error trying to write mountain to cache: {0}")]
    CacheIO(std::io::Error),

    #[error("Error joining spawned futures: {0}")]
    Join(#[from] tokio::task::JoinError),
}

type Result<T> = std::result::Result<T, ScrapError>;

/// Retrieves the DOM of a given URL
///
/// If the url is cached then return the cached data. Otherwise make a request
/// to the website, store it in cache and return it.
async fn get_dom(client: &Client, path: &str) -> Result<Html> {
    let cache_path = PathBuf::from(".cache")
        .join(path.strip_prefix('/').unwrap())
        .join("data");

    if let Ok(bytes) = fs::read(&cache_path).await {
        Ok(Html::parse_document(&String::from_utf8_lossy(&bytes)))
    } else {
        let text = client.get(format!("{SITE_URL}{path}"))
            .send().await?.text().await?;

        // store in the cache
        fs::create_dir_all(cache_path.parent().unwrap()).await
            .map_err(ScrapError::CacheIO)?;
        fs::write(&cache_path, text.as_bytes()).await
            .map_err(ScrapError::CacheIO)?;

        Ok(Html::parse_document(&text))
    }
}

async fn get_ranges(client: &Client) -> Result<Vec<String>> {
    log::debug!("Getting initial range list");
    let dom = get_dom(client, "/mountain_ranges").await?;
    let selector = Selector::parse(".b-list-table__item-name--ranges a").unwrap();

    Ok(
        dom.select(&selector)
            .filter_map(|item| item.value().attr("href"))
            .map(|href| href.to_owned())
            .collect()
    )
}

/// Given an URL that lists mountains return the list of links to them
async fn get_peak_links(client: &Client, path: &str) -> Result<Vec<String>> {
    log::debug!("Getting peak links for {path}");
    let dom = get_dom(client, path).await?;
    let selector = Selector::parse(".b-list-table__item-name--detail a").unwrap();

    Ok(
        dom.select(&selector)
            .filter_map(|item| item.value().attr("href"))
            .map(|href| href.to_owned())
            .collect()
    )
}

async fn get_subrange_links(client: &Client, path: &str) -> Result<Vec<String>> {
    log::debug!("Getting subrange links for {path}");

    let dom = get_dom(client, path).await?;
    let selector = Selector::parse(".b-list-table__item-name--ranges a").unwrap();

    Ok(
        dom.select(&selector)
            .filter_map(|item| item.value().attr("href"))
            .map(|href| href.to_owned())
            .collect()
    )
}

async fn get_peak_detail(client: &Client, path: &str) -> Result<Mountain> {
    log::debug!("Getting peak detail for {path}");
    let dom = get_dom(client, path).await?;
    let name_selector = Selector::parse("#location_filename_part option[selected]").unwrap();
    let altitudes_selector = Selector::parse(".forecast-table-elevation__link .height").unwrap();

    let name = dom.select(&name_selector)
        .next().ok_or_else(|| ScrapError::NameSelectorFailed(path.into()))?
        .text().next().unwrap()
        .to_owned();
    let altitudes: Vec<_> = dom.select(&altitudes_selector)
        .map(|el| el.text().next().unwrap().parse().unwrap()).collect();

    let altitudes = if altitudes.is_empty() {
        vec![path.split('/').last().unwrap().parse().unwrap()]
    } else {
        altitudes
    };

    let code = PathBuf::from(path)
        .parent().unwrap().parent().unwrap()
        .file_name().unwrap().to_string_lossy().to_string();

    Ok(Mountain {
        normalized_name: normalize(&name),
        id: MountainId::from_code(&code),
        code, name, altitudes,
    })
}

async fn the_main(client: &Client) -> Result<MountainFile> {
    // query the list of ranges
    let ranges_urls = get_ranges(client).await?;

    println!("Querying {} mountain ranges", ranges_urls.len());

    // spawn a task for every range to get its subranges
    let subranges = {
        let semaphore = Arc::new(Semaphore::new(MAX_CONCURRENT_REQUESTS));
        let handles = ranges_urls.iter().cloned().map(|path| {
            let client = client.clone();
            let semaphore = Arc::clone(&semaphore);

            tokio::spawn(async move {
                let _permit = semaphore.acquire().await.unwrap();
                get_subrange_links(&client.clone(), &path).await
            })
        });

        // join and flatten the tasks to get all the paths
        let mut subrange_paths = Vec::new();

        for res in try_join_all(handles).await? {
            match res {
                Ok(s) => subrange_paths.extend(s),
                Err(e) => log::error!("{e}"),
            }
        }

        subrange_paths
    };

    // spawn a task for every range and subrange to get the list of peaks
    let mountain_links = {
        let semaphore = Arc::new(Semaphore::new(MAX_CONCURRENT_REQUESTS));
        let handles = ranges_urls.into_iter().chain(subranges.into_iter()).map(|path| {
            let client = client.clone();
            let semaphore = Arc::clone(&semaphore);

            tokio::spawn(async move {
                let _permit = semaphore.acquire().await.unwrap();
                get_peak_links(&client.clone(), &path).await
            })
        });

        // join and flatten the tasks to get all the paths
        let mut mountain_paths = Vec::new();

        for res in try_join_all(handles).await? {
            match res {
                Ok(s) => mountain_paths.extend(s),
                Err(e) => log::error!("{e}"),
            }
        }

        mountain_paths
    };

    // spawn a task for every mountain path to get its details
    let mountains = {
        let semaphore = Arc::new(Semaphore::new(MAX_CONCURRENT_REQUESTS));
        let mountain_handles: Vec<_> = mountain_links.into_iter().map(|path| {
            let client = client.clone();
            let semaphore = Arc::clone(&semaphore);

            tokio::spawn(async move {
                let _permit = semaphore.acquire().await.unwrap();
                get_peak_detail(&client, &path).await
            })
        }).collect();

        println!("Expected {} peaks", mountain_handles.len());

        // join the mountain handles to get a vector of mountain details
        try_join_all(mountain_handles)
            .await?.into_iter().filter_map(|r| {
                match r {
                    Ok(m) => Some(m),
                    Err(e) => { log::error!("{e}"); None }
                }
            }).collect::<Vec<_>>()
    };

    println!("Got {} peaks", mountains.len());

    let mut output = MountainFile::new();

    for mountain in mountains {
        output.mountains.insert(mountain.id.clone(), mountain);
    }

    Ok(output)
}

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Where to output the mountain list
    #[clap(short, long)]
    output: String,
}

#[tokio::main(worker_threads = 10)]
async fn main() -> Result<()> {
    env_logger::init();

    let client = Client::new();
    let args = Args::parse();

    let mountains = the_main(&client).await?;

    // serialize the mountain details to the specified json file
    let json = serde_json::to_string_pretty(&mountains).unwrap();
    fs::write(args.output, json.as_bytes()).await.unwrap();

    Ok(())
}
