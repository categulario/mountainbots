use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Could not deserialize response from capture module.\nError: {error}\nOutput: {output}")]
    DeserializingCapModOutput {
        error: serde_json::Error,
        output: String,
    },

    #[error("Capture module failed.\nExit code: {code}\nStdout: {stdout}\nStderr: {stderr}")]
    CapModFailed {
        code: i32,
        stdout: String,
        stderr: String,
    },
}

pub type Result<T> = std::result::Result<T, Error>;
