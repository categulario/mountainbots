use mountains::models::Mountain;

pub fn no_mountain_found(account: &str) -> String {
    format!("\
@{account} Ninguna montaña coincide con tu búsqueda 🤷🏽‍♀️, qué raro. ¿Y \
si intentas otra?

A veces sirve quitarle partes a la búsqueda y dejar las cosas distintivas, por \
ejemplo en vez de buscar:

Monte tlaloc

puedes probar buscando:

tlaloc")
}

pub fn mountain_found(account: &str, mountain: &Mountain) -> String {
    let name = &mountain.name;
    let altitude = mountain.altitudes[0];
    let code = &mountain.code;

    format!("\
@{account} Aquí está el reporte del clima para 🏔️ {name} en su cima a {altitude} msnm 🎉.

Ve el reporte completo en https://www.mountain-forecast.com/peaks/{code}/forecasts/{altitude}
")
}

pub fn multiple_mountains_found(account: &str, mountains: &[&Mountain]) -> String {
    let list: Vec<String> = mountains
        .iter().enumerate().take(5)
        .map(|(i, m)| format!("{}. {}\n", i+1, m.name))
        .collect();
    let list = list.join("");

    format!("\
@{account} Varias montañas coinciden con tu búsqueda 🤯:

{list}

Si te interesa alguna en particuar arróbame de nuevo fuera de este hilo con \
uno de esos nombres. Responder en este mismo hilo no funciona porque ignoro \
las menciones en hilos para evitar spam.
")
}

pub fn error(account: &str) -> String {
    format!("\
@{account}

⚠️🤖 ups! el bot falló 🤦🏽‍♂️

No te preocupes, no fue tu culpa. A veces intentarlo de nuevo funciona, a \
veces no. Si lo intentas de nuevo recuerda que el bot no responde en hilos, \
así que tendrías que mencionarlo de nuevo en un toot.

(hey, pst, @categulario, ¿ya viste esto?)

#climaenelcerrofailed
")
}
