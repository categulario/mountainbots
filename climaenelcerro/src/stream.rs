use std::time::Duration;
use std::path::Path;

use futures::TryStreamExt;
use eventsource_client::{
    Client, ClientBuilder, ReconnectOptions, SSE, Error as ESError,
};
use mountains::models::MountainFile;
use mountains::text::{r#match, Match};
use mountains::get_capture;
use tokio_util::codec::{BytesCodec, FramedRead};
use tokio::fs::File;
use reqwest::{Body, multipart::{Part, Form}};

use crate::models::{Notification, Media, StatusRequest};
use crate::clean::clean_content;
use crate::settings::Settings;
use crate::messages;

pub async fn run_stream(settings: &'static Settings, mountain_list: &'static MountainFile) -> Result<(), ESError> {
    let access_token = &settings.access_token;
    let api_endpoint = &settings.api_endpoint;
    let client = ClientBuilder::for_url(&format!("{api_endpoint}/streaming/user/notification"))?
        .header("Authorization", &format!("Bearer {access_token}"))?
        .reconnect(
            ReconnectOptions::reconnect(true)
                .retry_initial(false)
                .delay(Duration::from_secs(1))
                .backoff_factor(2)
                .delay_max(Duration::from_secs(60))
                .build(),
        )
        .build();

    let mut stream = client.stream();

    log::info!("Initiating listen loop");

    while let Some(event) = stream.try_next().await? {
        match event {
            SSE::Event(ev) if ev.event_type == "notification" => {
                let data = &ev.data;

                match serde_json::from_str(data) {
                    Ok(notif) => {
                        tokio::spawn(async move {
                            handle_event(notif, settings, mountain_list).await;
                        });
                    }
                    Err(e) => log::error!("Error parsing notification: {e}"),
                }
            }
            SSE::Event(ev) => {
                let r#type = ev.event_type;
                log::debug!("unhandled event {type:?}");
            }
            SSE::Comment(_) => {}
        }
    }

    Ok(())
}

async fn reply(settings: &Settings, client: &reqwest::Client, status: StatusRequest) -> reqwest::Result<reqwest::Response> {
    let api_endpoint = &settings.api_endpoint;
    let access_token = &settings.access_token;

    let res = client.post(format!("{api_endpoint}/statuses"))
        .bearer_auth(access_token)
        .form(&status)
        .send().await.unwrap();

    res.error_for_status()
}

async fn media(settings: &Settings, client: &reqwest::Client, path: impl AsRef<Path>) -> Media {
    let api_endpoint = &settings.api_endpoint;
    let access_token = &settings.access_token;
    let file = File::open(path).await.unwrap();

    // read file body stream
    let stream = FramedRead::new(file, BytesCodec::new());
    let file_body = Body::wrap_stream(stream);

    //make form part of file
    let some_file = Part::stream(file_body)
        .file_name("image.png")
        .mime_str("text/plain").unwrap();

    //create the multipart form
    let form = Form::new()
        .text("username", "seanmonstar")
        .text("password", "secret")
        .part("file", some_file);

    let res = client.post(format!("{api_endpoint}/media"))
        .bearer_auth(access_token)
        .multipart(form)
        .send().await.unwrap();

    res.error_for_status().unwrap().json().await.unwrap()
}

async fn handle_event(notif: Notification, settings: &Settings, mountains: &MountainFile) {
    let client = reqwest::Client::new();
    let r#type = &notif.r#type;

    // bellow check that the status is not a reply so that the bot does not
    // reply to replies.
    if r#type == "mention" && notif.status.in_reply_to_id.is_none() {
        let content = clean_content(&notif.status.content, settings);
        let acct = &notif.account.acct;
        log::info!("Got mention from {acct} with text: {content}");

        match r#match(&content, mountains) {
            Match::None => {
                let status = notif.status
                    .reply_with(messages::no_mountain_found(acct));
                reply(settings, &client, status).await.unwrap();
                log::info!("@{acct}'s request not found: '{content}'");
            }
            Match::Single(mountain) => {
                let result = get_capture(mountain, mountain.altitudes[0], &settings.capture_settings).await;

                match result {
                    Ok(capture_path) => {
                        let media = media(settings, &client, capture_path).await;
                        let status = notif.status
                            .reply_with(messages::mountain_found(acct, mountain))
                            .with_media(media);
                        reply(settings, &client, status).await.unwrap();
                        log::info!("Successfully responded to @{acct}'s query for '{content}'");
                    }
                    Err(error) => {
                        let status = notif.status
                            .reply_with(messages::error(acct));
                        reply(settings, &client, status).await.unwrap();
                        log::error!("{error}");
                    }
                }
            }
            Match::Multiple(mountains) => {
                let status = notif.status
                    .reply_with(messages::multiple_mountains_found(acct, &mountains));
                reply(settings, &client, status).await.unwrap();
                log::info!("Multiple mountains found for @{acct}' query for '{content}'");
            }
        }
    } else {
        log::debug!("Got notification type: {type}");
    }
}
