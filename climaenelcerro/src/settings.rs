use serde::Deserialize;

use mountains::settings::Settings as CaptureSettings;

use crate::models::StatusVisibility;

#[derive(Deserialize)]
pub struct Settings {
    pub bot_name: String,
    pub access_token: String,
    pub api_endpoint: String,
    pub default_visibility: StatusVisibility,

    pub capture_settings: CaptureSettings,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            bot_name: String::from("@climaenelcerro"),
            access_token: String::new(),
            api_endpoint: String::from("https://mstdn.mx/api/v1"),
            capture_settings: Default::default(),
            default_visibility: Default::default(),
        }
    }
}
