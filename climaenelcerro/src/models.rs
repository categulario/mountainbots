use serde::{Serialize, Deserialize};
use clap::ValueEnum;

#[derive(Debug, Deserialize)]
pub struct Notification {
    pub id: String,
    pub r#type: String,
    pub status: Status,
    pub account: Account,
}

#[derive(Debug, Deserialize)]
pub struct Status {
    pub id: String,
    pub content: String,
    pub in_reply_to_id: Option<String>,
    pub visibility: StatusVisibility,
}

impl Status {
    pub fn reply_with(&self, text: String) -> StatusRequest {
        StatusRequest {
            status: text,
            in_reply_to_id: Some(self.id.to_owned()),
            visibility: self.visibility,
            media_ids: None,
        }
    }
}

#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, Serialize, Deserialize, ValueEnum)]
#[serde(rename_all="snake_case")]
pub enum StatusVisibility {
    /// Statuses are seen by everybody
    Public,

    /// Statuses are visible but not shown in timelines
    #[default]
    Unlisted,

    /// Only followers see statuses
    Private,

    /// Only mentioned accounts see statuses
    Direct,
}

#[derive(Debug, Serialize)]
pub struct StatusRequest {
    status: String,
    in_reply_to_id: Option<String>,
    visibility: StatusVisibility,

    #[serde(rename="media_ids[]")]
    media_ids: Option<String>,
}

impl StatusRequest {
    pub fn with_media(self, media: Media) -> StatusRequest {
        StatusRequest {
            media_ids: Some(media.id),
            ..self
        }
    }

    pub fn with_visibility(self, visibility: StatusVisibility) -> StatusRequest {
        StatusRequest {
            visibility,
            ..self
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct Account {
    pub acct: String,
}

#[derive(Debug, Deserialize)]
pub struct Media {
    pub id: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reply_with_the_same_visibility() {
        let status = Status {
            id: "".into(), content: "".into(), in_reply_to_id: None,
            visibility: StatusVisibility::Direct,
        };

        let StatusRequest { visibility, .. } = status.reply_with("de".into());

        assert_eq!(visibility, StatusVisibility::Direct);
    }

    #[test]
    fn deserialize_media() {
        let Media { id } = serde_json::from_str(include_str!("../assets/test/payloads/media.json")).unwrap();

        assert_eq!(id, "110353727991355247");
    }

    #[test]
    fn deserialize_notification() {
        let Notification {
            id, r#type,
            status: Status {
                id: status_id, content, in_reply_to_id, visibility,
            },
            account: Account { acct },
        } = serde_json::from_str(include_str!("../assets/test/payloads/notif.json")).unwrap();

        assert_eq!(id, "357801");
        assert_eq!(r#type, "mention");
        assert_eq!(acct, "categulario");

        assert_eq!(status_id, "110223245639441788");
        assert_eq!(content, "<p>hola <span class=\"h-card\"><a href=\"https://mstdn.mx/@climaenelcerro\" class=\"u-url mention\">@<span>climaenelcerro</span></a></span></p>");
        assert_eq!(in_reply_to_id, None);
        assert_eq!(visibility, StatusVisibility::Public);
    }
}
