// check https://docs.joinmastodon.org/methods/streaming/
use std::path::{PathBuf, Path};
use std::fs::read_to_string;

use clap::Parser;
use mountains::models::MountainFile;

use climaenelcerro::stream::run_stream;
use climaenelcerro::error::{Error, Result};
use climaenelcerro::settings::Settings;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long, value_name="FILE")]
    config: PathBuf,

    #[clap(short, long, value_name="FILE")]
    mountains: PathBuf,
}

fn read_settings(filename: &Path) -> Result<Settings> {
    let contents = read_to_string(filename).map_err(Error::ReadingSettings)?;

    toml::from_str(&contents).map_err(Error::ParsingSettings)
}

fn read_mountain_list(filename: &Path) -> Result<MountainFile> {
    let contents = read_to_string(filename).map_err(Error::ReadingMountains)?;

    serde_json::from_str(&contents).map_err(Error::ParsingMountains)
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::builder()
        .filter(Some("climaenelcerro"), log::LevelFilter::Debug)
        .filter(Some("mountains"), log::LevelFilter::Debug)
        .filter(Some("eventsource_client"), log::LevelFilter::Info)
        .format_timestamp(None)
        .parse_default_env()
        .init();

    let args = Args::parse();
    let settings: &'static _ = Box::leak(Box::new(read_settings(&args.config)?));
    let mountain_list: &'static _ = Box::leak(Box::new(read_mountain_list(&args.mountains)?));

    run_stream(settings, mountain_list).await.map_err(Error::EventSource)
}
