#!/bin/bash

__dir=$(dirname $(realpath $0))

container=climaenelcerro
image=registry.gitlab.com/categulario/mountainbots/mountainbots

if [[ -z $@ ]]; then
    cmd=$image
    name="--name=$container"

    podman rm -i $container
else
    cmd="-it $image $@"
fi

podman run -it --rm \
    --entrypoint /usr/bin/climaenelcerro \
    --uidmap 900:0:1 --uidmap 0:1:900 \
    --volume $__dir/settings-climaenelcerro.toml:/etc/climaenelcerro/settings.toml:U \
    --volume $__dir/../../mountains_new.json:/var/lib/mountainbots/mountains.json:U \
    $name \
    $cmd \
    --config /etc/climaenelcerro/settings.toml \
    --mountains /var/lib/mountainbots/mountains.json
