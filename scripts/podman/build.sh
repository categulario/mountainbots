#!/bin/bash

__dir=$(dirname $(realpath $0))

podman build \
    -t registry.gitlab.com/categulario/mountainbots/mountainbots \
    -f $__dir/../../Containerfile \
    $__dir/../../
