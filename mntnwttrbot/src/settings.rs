use std::path::PathBuf;

use serde::Deserialize;
use mountains::settings::Settings as CaptureSettings;

#[derive(Clone, Deserialize)]
#[serde(default)]
pub struct Settings {
    pub token: String,

    #[serde(flatten)]
    pub capture_settings: CaptureSettings,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            token: "123456".to_owned(),
            capture_settings: CaptureSettings {
                node_path: PathBuf::from("node"),
                capture_script_path: PathBuf::from("main.js"),
                capture_cache_dir: PathBuf::from("capture_cache"),
            },
        }
    }
}
