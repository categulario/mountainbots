pub const MSG_HELLO: &str = r"Hello There 👋🏽\! Send me a mountain name and I'll try to find its weather for you";

pub const MSG_INVALID_QUERY: &str = r"I couldn't undestand your query 🤕, but try me with something like 'Cofre de perote' 😉";

pub const MSG_NO_MOUNTAIN_FOUND: &str = r"Whoops\! there's no mountain in the list with such name 🤷‍♀️\. How weird\. Try another one 😉";

pub const MSG_MANY_MOUNTAINS_FOUND: &str = r"More than one mountain matches your query 😵\. Here are some of them 👇";

pub const MSG_OLD_BUTTON_PRESSED: &str = "I couldn't undestand the button you just pressed 😕 Perhaps it belonged to an older message 👵?";

pub const MSG_SUCCESS_MOUNTAIN_FORECAST: &str = r"🎉 Here is the weather report for 🗻 *{mountain}* at ⬆️ *{altitude}* meters\. See full report at {link}\.";

pub const MSG_OTHER_ALTITUDES: &str = r"Other altitudes for the same mountain you might be interested in 🙂👇";

pub const MSG_GETTING_CAPTURE: &str = r"🗻 Mountain found\! getting weather ⏳";

pub const MSG_CAPTURE_FAILED: &str = r"⚠️ Getting the weather failed 🤕 It is not your fault";
