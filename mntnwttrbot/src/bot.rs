use std::path::PathBuf;

use anyhow::Result;
use teloxide::prelude::*;
use teloxide::adaptors::DefaultParseMode;
use teloxide::types::{
    ReplyMarkup, InlineKeyboardButton, InlineKeyboardButtonKind, InputFile,
};
use strfmt::strfmt;
use mountains::models::{Mountain, MountainFile};
use mountains::get_capture;

use crate::settings::Settings;
use crate::core::{Query, Response, respond};
use crate::msgs::*;
use crate::teloxide_bridge::clean;

pub struct AppData {
    pub settings: Settings,
    pub mountain_list: MountainFile,
}

async fn send_report(bot: &DefaultParseMode<Bot>, chat_id: ChatId, path: PathBuf, mountain: &Mountain, altitude: u16) -> Result<()> {
    let code = clean(&mountain.code);
    let mntnname = clean(&mountain.name);

    let msg = strfmt(MSG_SUCCESS_MOUNTAIN_FORECAST, &[
        ("mountain".to_string(), mntnname),
        ("altitude".to_string(), altitude.to_string()),
        ("link".to_string(), format!("https://www\\.mountain\\-forecast\\.com/peaks/{code}/forecasts/{altitude}")),
    ].into_iter().collect())?;

    // send the picture with the weather and a caption with info
    bot
        .send_photo(chat_id, InputFile::file(path))
        .caption(msg)
        .send().await?;

    // Create an inline keyboard with other altitudes from the same mountain
    let inline_keyboard = ReplyMarkup::inline_kb(
        mountain.altitudes.chunks(3).map(|altitudes| {
            altitudes.iter().map(|altitude| {
                let id = &mountain.id;

                InlineKeyboardButton::new(
                    format!("⬆️ {altitude}"),
                    InlineKeyboardButtonKind::CallbackData(format!("m:{id}:{altitude}"))
                )
            }).collect::<Vec<_>>()
        })
    );

    // send a message with some info
    bot
        .send_message(chat_id, MSG_OTHER_ALTITUDES)
        .reply_markup(inline_keyboard)
        .send().await?;

    Ok(())
}

async fn respond_logic(bot: &DefaultParseMode<Bot>, chat_id: ChatId, mountain: &Mountain, altitude: u16, settings: &Settings) -> Result<()> {
    let wait_msg = bot
        .send_message(chat_id, MSG_GETTING_CAPTURE).send().await.unwrap();

    let cap = get_capture(mountain, altitude, &settings.capture_settings).await;

    bot
        .delete_message(chat_id, wait_msg.id)
        .send().await.unwrap();

    match cap {
        Ok(path) => {
            send_report(bot, chat_id, path, mountain, altitude).await.unwrap();
        },
        Err(e) => {
            bot
                .send_message(chat_id, MSG_CAPTURE_FAILED)
                .send().await.unwrap();

            log::error!("{e}");
        }
    }

    Ok(())
}

async fn message_handler(bot: DefaultParseMode<Bot>, message: Message, data: &'static AppData) -> Result<()> {
    match Query::try_from(&message) {
        Ok(q) => match respond(&q, &data.mountain_list) {
            Response::Hello => {
                bot
                    .send_message(message.chat.id, MSG_HELLO)
                    .await?;
            }

            Response::Empty => {
                bot
                    .send_message(message.chat.id, MSG_NO_MOUNTAIN_FOUND)
                    .reply_to_message_id(message.id)
                    .await?;
            }

            Response::MountainList(mountains) => {
                let inline_keyboard = ReplyMarkup::inline_kb(
                    mountains.into_iter().map(|m| {
                        let altitude = m.altitudes[0];
                        let id = m.id;

                        vec![InlineKeyboardButton::new(
                            format!("🗻 {} ({})", m.name, altitude),
                            InlineKeyboardButtonKind::CallbackData(format!("m:{id}:{altitude}"))
                        )]
                    })
                );

                bot
                    .send_message(message.chat.id, MSG_MANY_MOUNTAINS_FOUND)
                    .reply_to_message_id(message.id)
                    .reply_markup(inline_keyboard)
                    .await?;
            }

            Response::WeatherReport { mountain, altitude } => {
                respond_logic(
                    &bot,
                    message.chat.id,
                    &mountain,
                    altitude,
                    &data.settings,
                ).await.unwrap();
            }
        },
        Err(_) => {
            bot
                .send_message(message.chat.id, MSG_INVALID_QUERY)
                .reply_to_message_id(message.id)
                .await?;
        }
    }

    Ok(())
}

async fn callback_handler(bot: DefaultParseMode<Bot>, callback_query: CallbackQuery, data: &'static AppData) -> Result<()> {
    let message = callback_query.message;
    let payload = callback_query.data;

    if let (Some(msg), Some(payload)) = (message, payload) {
        bot.delete_message(msg.chat.id, msg.id).send().await.unwrap();

        match Query::parse_inline_data(&payload) {
            Ok(query) => {
                if let Query::Button { mountain_id, altitude } = query {
                    let mountain = &data.mountain_list.by_id(&mountain_id);

                    if let Some(mountain) = mountain {
                        respond_logic(
                            &bot,
                            msg.chat.id,
                            mountain,
                            altitude,
                            &data.settings,
                        ).await.unwrap();
                    } else {
                        bot.send_message(msg.chat.id, "Mountain not found. Did you press an old button?").send().await.unwrap();
                    }
                }
            }
            Err(description) => {
                bot.send_message(msg.chat.id, description).send().await.unwrap();
            }
        }
    }

    Ok(())
}

async fn run(data: &'static AppData) -> Result<()> {
    log::info!("Starting bot...");

    let bot = Bot::new(&data.settings.token)
        .parse_mode(teloxide::types::ParseMode::MarkdownV2);

    let handler = dptree::entry()
        .branch(Update::filter_message().endpoint(message_handler))
        .branch(Update::filter_callback_query().endpoint(callback_handler));

    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![data])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;

    Ok(())
}

#[tokio::main]
pub async fn bot_main(data: AppData) -> Result<()> {
    let static_ref: &'static _ = Box::leak(Box::new(data));

    run(static_ref).await
}
