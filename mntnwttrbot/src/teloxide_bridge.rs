use std::convert::TryFrom;

use teloxide::types::Message;

use crate::core::Query;

impl TryFrom<&Message> for Query {
    type Error = ();

    fn try_from(update: &Message) -> Result<Query, Self::Error> {
        if let Some(text) = update.text() {
            match text {
                "/start" => Ok(Query::Start),
                t => Ok(Query::text(t)),
            }
        } else {
            Err(())
        }
    }
}

const BAD_CHARS: &[char] = &['_', '*', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!'];

pub fn clean(s: &str) -> String {
    s.chars().map(|c| {
        if BAD_CHARS.contains(&c) {
            format!("\\{c}")
        } else {
            c.to_string()
        }
    }).collect()
}
