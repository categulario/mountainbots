#[macro_use]
extern crate lazy_static;

pub mod bot;
pub mod settings;
pub mod core;
pub mod teloxide_bridge;
pub mod msgs;
