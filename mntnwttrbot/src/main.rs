use std::fs::read_to_string;
use std::path::PathBuf;

use clap::Parser;
use anyhow::{Result, Error, Context};
use mountains::models::MountainFile;

use mntnwttrbot::bot::{AppData, bot_main};
use mntnwttrbot::settings::Settings;

fn read_settings(filename: &str) -> Result<Settings> {
    let contents = read_to_string(filename)
        .context(format!("Settings file at {filename} could not be read. Do you have read permissions on it?"))?;

    toml::from_str(&contents).context("Config file could not be parsed")
}

fn read_mountain_list(filename: &str) -> Result<MountainFile> {
    let contents = read_to_string(filename)
        .context("Specified mountain list file exists but could not be read. Do you have read permissions on it?")?;

    serde_json::from_str(&contents).context("Mountain list could not be parsed")
}

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// This service's config file
    #[clap(short, long)]
    config: String,

    /// A list of mountains in JSON format
    #[clap(short, long)]
    mountains: String,
}

fn main() -> Result<()> {
    env_logger::builder()
        .filter(Some("mntnwttrbot"), log::LevelFilter::Debug)
        .filter(Some("teloxide"), log::LevelFilter::Debug)
        .format_timestamp(None)
        .init();

    let args = Args::parse();
    let settings = read_settings(&args.config)?;
    let mountain_list = read_mountain_list(&args.mountains)?;

    if !PathBuf::from(&settings.capture_settings.capture_script_path).exists() {
        return Err(Error::msg(format!("
The given path to the capture script does not exist.
The path is: {:?}
", &settings.capture_settings.capture_script_path)));
    }

    let capture_cache_dir = &settings.capture_settings.capture_cache_dir;
    log::info!("Clearing cache dir at {capture_cache_dir:?}");
    if std::fs::remove_dir_all(&settings.capture_settings.capture_cache_dir).is_err() {
        log::info!("There was no cache dir, will be created by capture service");
    }

    let data = AppData {
        settings, mountain_list,
    };

    bot_main(data).context("Failed to launch actix")?;

    Ok(())
}
