//! The core logic of the bot. Everything else are implementation details.
//!
//! The logic is very simple. A function that takes input from the user and
//! returns the expected output from the bot in the form of a Response enum
//! variant.

use regex::Regex;
use mountains::models::{Mountain, MountainId, MountainFile};
use mountains::text::{Match, r#match};

use crate::msgs::MSG_OLD_BUTTON_PRESSED;

lazy_static! {
    static ref MNTN_CALLBACK_REGEX: Regex = Regex::new(r"m:(?P<id>[a-zA-Z0-9_-]+):(?P<altitude>\d+)").unwrap();
}

/// The Response given by the bot
#[derive(Debug, PartialEq)]
pub(crate) enum Response {
    Hello,

    /// A mountain and altitude were specified, so the response is a full report
    /// with the option of choosing some other altitude to further query the
    /// weather.
    WeatherReport {
        mountain: Mountain,
        altitude: u16,
    },

    /// The query was not specific enough so a list of possible mountains is
    /// returned.
    MountainList(Vec<Mountain>),

    /// No match was found, sugest something else
    Empty,
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum Query {
    /// Very first message
    Start,

    /// The user sent a text message
    Text {
        msg: String,
    },

    /// The user clicked on a button
    Button {
        mountain_id: MountainId,
        altitude: u16,
    },
}

impl Query {
    pub fn text(msg: &str) -> Query {
        Query::Text {
            msg: msg.into(),
        }
    }

    pub fn parse_inline_data(data: &str) -> Result<Query, &'static str> {
        if let Some(caps) = MNTN_CALLBACK_REGEX.captures(data) {
            Ok(Query::Button {
                mountain_id: MountainId::from(&caps["id"]),
                altitude: (caps["altitude"]).parse().map_err(|_| MSG_OLD_BUTTON_PRESSED)?,
            })
        } else {
            Err(MSG_OLD_BUTTON_PRESSED)
        }
    }
}

pub(crate) fn respond(query: &Query, mountain_list: &MountainFile) -> Response {
    match query {
        Query::Start => Response::Hello,
        Query::Text { msg, .. } => {
            match r#match(msg, mountain_list) {
                Match::None => Response::Empty,
                Match::Single(mountain) => {
                    Response::WeatherReport {
                        mountain: mountain.clone(),
                        altitude: mountain.altitudes[0],
                    }
                }
                Match::Multiple(matches) => Response::MountainList({
                    let mut list: Vec<_> = matches.into_iter().take(5).cloned().collect();

                    list.sort();

                    list
                }),
            }
        }

        Query::Button { .. } => unimplemented!(),
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    use mountains::text::normalize;
    use mountains::models::MountainId;

    use super::*;

    fn make_mountain(name: &str) -> Mountain {
        Mountain {
            name: name.into(),
            normalized_name: normalize(name),
            code: name.into(),
            altitudes: vec![4283],
            id: MountainId::from_code(name),
        }
    }

    #[test]
    fn with_only_one_match_specific_mountain_is_returned() {
        let query = Query::text("a");
        let a = make_mountain("a");
        let mountains = MountainFile::from_mountains([
            a.clone(), make_mountain("b"), make_mountain("c"),
        ]);

        assert_eq!(respond(&query, &mountains), Response::WeatherReport {
            mountain: a,
            altitude: 4283,
        });
    }

    #[test]
    fn normalized_search() {
        let query = Query::text("á.Ma");
        let a = make_mountain("ama");
        let mountains = MountainFile::from_mountains([a.clone()]);

        assert_eq!(respond(&query, &mountains), Response::WeatherReport {
            mountain: a,
            altitude: 4283,
        });
    }

    #[test]
    fn with_multiple_matches_options_are_returned() {
        let query = Query::text("arr");
        let arroz = make_mountain("arroz");
        let arriba = make_mountain("arriba");
        let mountains = MountainFile::from_mountains([
            arroz.clone(), arriba.clone(), make_mountain("berro"), make_mountain("cebolla"),
        ]);

        assert_eq!(respond(&query, &mountains), Response::MountainList(vec![
            arriba, arroz,
        ]));
    }

    #[test]
    fn parse_inline_data() {
        assert_eq!(Query::parse_inline_data("m:123:1020").unwrap(), Query::Button {
            mountain_id: MountainId::from("123"),
            altitude: 1020,
        });
    }
}
