# Mountain weather forecast bot

Ask this bot for a mountain's weather forecast.

## Setting up

* you need [rust](https://rustup.rs)
* and [node](https://nodejs.org) (tested against v18)
* then inside the `mountains/capture` directory do `npm install`.

## How to run

You need a config file like this:

```toml
# settings.toml
token = "your telegram token"
capture_script_path = "./mountains/capture/main.js"
capture_cache_dir = "./capture_cache"
node_path = "node"
```

And a `mountains.json` file (see the next section on how to get it).

Then you can run it with this:

    cargo run --bin mntnwttrbot -- -c settings.toml -m mountains.json
