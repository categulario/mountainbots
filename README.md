# Mountain bots

Currently this cargo workspace contains a telegram bot, a mastodon bot, and a
library that powers both.

Their job is to answer for queries about the weather in a given mountain in
their respective platforms.

Check each individual project for information on how to build.

## Test the capture module

This will only run some javascript and a headless chrome browser.

* `cd mountains/capture`
* `node main.js -c ../../capture_cache/ -m 'Iztaccihuatl' -a 5286`

## Release a bot version

(this is for me)

* ensure tests and clippy pass
* Tag the changes with the new version
* Compile the new image
* Tag it
* Upload the image to the registry
* Update image tag in salt, and run `state.apply`
